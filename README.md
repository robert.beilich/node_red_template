node_red_template_project
===============================

Basic template project for version controlled node-red

### About

This is your project's README.md file. It helps users understand what your
project does, how to use it and anything else they may need to know.

### Startup

* `npm install` or `yarn` to install dependencies
* `npm run start` or `yarn start` to start the flow
* `npm run dev` or `yarn dev` to enable the editor for development
